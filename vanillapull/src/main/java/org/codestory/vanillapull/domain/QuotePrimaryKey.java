package org.codestory.vanillapull.domain;


public class QuotePrimaryKey {

	private String symbol;
	private Long ticker;

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public Long getTicker() {
		return ticker;
	}

	public void setTicker(Long ticker) {
		this.ticker = ticker;
	}

}
