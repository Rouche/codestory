package org.codestory.vanillapull.service;

import java.util.ArrayList;
import java.util.List;

import org.codestory.vanillapull.InstrumentDao;
import org.codestory.vanillapull.domain.Instrument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/instrument")
public class InstrumentService {

    @Autowired
    private InstrumentDao instrumentDAO;

    @Override
    public List<InstrumentDisplay> getInstrumentList() {
        // Retrieve instruments configuration
        List<Instrument> insList = instrumentDAO.findAll();

        // Retrieve instrument after instrument
        List<InstrumentDisplay> returnList = new ArrayList<InstrumentDisplay>();
        for (Instrument ins : insList) {
            InstrumentDisplay insDisplay = new InstrumentDisplay();
            insDisplay.setSymbol(ins.getSymbol());
            insDisplay.setLabel(ins.getLabel());
            returnList.add(insDisplay);
        }

        return returnList;
    }
}