package org.codestory.vanillapull;

import org.codestory.vanillapull.service.InstrumentService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {

    @Bean
    public InstrumentService instrumentService() {
        return new InstrumentService();
    }
}
